package tn.esprit.spring;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.services.IDepartementService;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.repository.EntrepriseRepository;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.services.ITimesheetService;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.services.ContratServiceImpl;



@SpringBootTest
public class TimesheetApplicationTests {


	private static final Logger l = LoggerFactory.getLogger(TimesheetApplicationTests.class);

	@Autowired
	DepartementRepository deptRepoistory;

	@Autowired
	IDepartementService idepartementService;

	@Autowired
	IEntrepriseService entrepriseService;

	@Autowired
	ContratRepository contratRepository;
	@Autowired
	ContratServiceImpl contratService;

	@Autowired
	MissionRepository missionRepository;

	@Autowired
	ITimesheetService iTimesheetService;

	@Autowired
	EntrepriseRepository entrepriseRepository;

	@Autowired
	IEmployeService iEmployeService;


	@Test
	public void TestAddContrat() {
		Contrat contrat = new Contrat(23);
		Long dataPreTest = contratRepository.count();
		Contrat savedContrat = contratRepository.save(contrat);
		Long dataAfterTest = contratRepository.count();
		assertThat(dataPreTest).isEqualTo(dataAfterTest - 1);
		assertThat(savedContrat.getId()).isEqualTo(contrat.getId());
		l.info("add contrat : " + contrat);

	}

	@Test
	void testaddOrUpdateEmploye() {
		Employe employe = new Employe("bourara","adnen","adnen.bourara@esprit.com","123456",true, Role.TECHNICIEN);
		int nmbrEmployeeBefore = iEmployeService.getNombreEmployeJPQL();
		int savedEmployeId =  iEmployeService.addOrUpdateEmploye(employe);
		int nmbrEmployeeAfter = iEmployeService.getNombreEmployeJPQL();
		Assert.assertEquals(nmbrEmployeeBefore,nmbrEmployeeAfter-1);
		iEmployeService.deleteEmployeById(savedEmployeId);
	}

	@Test
	void testgetEmployePrenomById() {
		String prenom = "adnen";
		Employe employe = new Employe("bourara",prenom,"adnen.bourara@esprit.com","123456",true, Role.TECHNICIEN);
		int savedEmployeId =  iEmployeService.addOrUpdateEmploye(employe);
		String getPrenom = iEmployeService.getEmployePrenomById(savedEmployeId);
		Assert.assertEquals(prenom,getPrenom);
		iEmployeService.deleteEmployeById(savedEmployeId);
	}

	@Test
	public void testAddEntreprise() {
		Entreprise entreprise = new Entreprise("Vermeg", "boite de developpement");
		Long dataPreTest = entrepriseRepository.count();
		Entreprise savedEntreprise = entrepriseRepository.save(entreprise);
		Long dataAfterTest = entrepriseRepository.count();
		assertThat(dataPreTest).isEqualTo(dataAfterTest - 1);
		assertThat(savedEntreprise.getId()).isEqualTo(savedEntreprise.getId());
		l.info("add entreprise : " + entreprise);
		entrepriseService.deleteEntrepriseById(entreprise.getId());
	}

	@Test
	public void testSupprimerEntreprise() {
		Entreprise entreprise = new Entreprise("Vermeg", "boite de developpement");
		Entreprise savedEntreprise = entrepriseRepository.save(entreprise);
		Long dataPreTest = entrepriseRepository.count();
		entrepriseService.deleteEntrepriseById(savedEntreprise.getId());
		Long dataAfterTest = entrepriseRepository.count();
		assertThat(dataPreTest).isEqualTo(dataAfterTest + 1);
		l.info(" this entreprise has been deleted : " + entreprise);
	}



	@Test
	public void testAddMission() {
		Mission mission = new Mission("Consulting", "Testing and Quality Assurance");
		Long dataPreTest = missionRepository.count();
		Mission savedMission = missionRepository.save(mission);
		Long dataAfterTest = missionRepository.count();
		assertThat(dataPreTest).isEqualTo(dataAfterTest - 1);
		assertThat(savedMission.getId()).isEqualTo(mission.getId());
		l.info("add mission : " + mission);
		iTimesheetService.deleteMissionById(mission.getId());
	}

	@Test
	public void testSupprimerMission() {
		Mission mission = new Mission("Consulting", "Testing and Quality Assurance");
		Mission savedMission = missionRepository.save(mission);
		Long dataPreTest = missionRepository.count();
		iTimesheetService.deleteMissionById(savedMission.getId());
		Long dataAfterTest = missionRepository.count();
		assertThat(dataPreTest).isEqualTo(dataAfterTest + 1);
		l.info(" this mission has been deleted : " + mission);
	}


	@Test
	public void testaddDepartement() {
		Departement dep = new Departement("Informatique");
		int dataPreTest = (int) deptRepoistory.count();
		int savedepart = idepartementService.ajouterDepartement(dep);
		int dataAfterTest = (int) deptRepoistory.count();
		Assert.assertEquals(dataPreTest,dataAfterTest-1);


		l.info(" Departement added : " + dep);
		idepartementService.deleteDepartementById(savedepart);
	}
	@Test
	public void TestDeleteDepartement() {
		Departement dep = new Departement("Informatique");
		Departement savedepart = deptRepoistory.save(dep);
		int dataPreTest = (int) deptRepoistory.count();
		idepartementService.deleteDepartementById(savedepart.getId());
		int dataAfterTest = (int) deptRepoistory.count();
		Assert.assertEquals(dataPreTest,dataAfterTest + 1);
		l.info(" this departement has been deleted : " + dep);
	}



	@Test
	public void TestDeleteContrat() {
		Contrat contrat = new Contrat(0);
		Contrat savedContrat = contratRepository.save(contrat);
		contratService.deleteAll();
		l.info(" this contrat has been deleted : " + contrat);
		assertThat(contratService.getAllContrats()).isEmpty();

	}



}