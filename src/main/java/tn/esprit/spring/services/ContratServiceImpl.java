package tn.esprit.spring.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.repository.ContratRepository;

@Service
public class ContratServiceImpl implements IContratService {


	@Autowired
	ContratRepository contratRepository;
	Contrat contrat;
	private static final Logger L = LogManager.getLogger(ContratServiceImpl.class);

	public List<Contrat> getAllContrats() {
		return (List<Contrat>) contratRepository.findAll();
	}
@Override
	public int ajouterContrat(Contrat contrat) {

		L.info("Start addContrat().");
		L.debug("Add Contrat : " + contrat.getReference());
		contratRepository.save(contrat);
		L.debug("Contrat " + contrat.getReference() + " added.");
		L.info("End ajouterContrat().");
		return contrat.getReference();
	}
	@Override
	public void deleteContratById(int contratId) {


			L.info("Start deleteContratById().");
		Optional<Contrat> oc = contratRepository.findById(contrat.getId());
		if(oc.isPresent()) {
			contratRepository.delete(oc.get());

			L.info("End deleteContratById().");
		}
		}
 public void deleteAll(){
		contratRepository.deleteAll();
 }
}

