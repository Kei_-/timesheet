package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;

@Service
public class EmployeServiceImpl implements IEmployeService {

    private static final Logger L = LoggerFactory.getLogger(EmployeServiceImpl.class);

    @Autowired
    EmployeRepository employeRepository;
    @Autowired
    DepartementRepository deptRepoistory;
    @Autowired
    ContratRepository contratRepoistory;
    @Autowired
    TimesheetRepository timesheetRepository;

    @Override
    public Employe authenticate(String login, String password) {
        return employeRepository.getEmployeByEmailAndPassword(login, password);
    }

    @Override
    public int addOrUpdateEmploye(Employe employe) {
        L.info("Start addOrUpdateEmploye().");
        String action;
        if (employe.getId() == 0) {
            L.debug("Add Employee : " + employe.getPrenom() + " " + employe.getNom());
            action = "Add";
        } else {
            L.debug("Update Employee : " + employe.getId());
            action = "Update";
        }
        employeRepository.save(employe);
        if (action.equals("Add")) {
            L.debug("Employee " + employe.getId() + " added.");
        } else {
            L.debug("Employee " + employe.getId() + " updated.");
        }
        L.info("End addOrUpdateEmploye().");
        return employe.getId();
    }


    public void mettreAjourEmailByEmployeId(String email, int employeId) {
        L.info("Start mettreAjourEmailByEmployeId().");

        L.debug("Getting Employee By Id = " + employeId);
        Optional<Employe> employeOpt = employeRepository.findById(employeId);
        if (employeOpt.isPresent()) {
            Employe employe = employeOpt.get();
            L.debug("Employee Before modification = " + employe);
            employe.setEmail(email);
            L.debug("Employee email Updated.");
            L.debug("Saving updated employee object.");
            employeRepository.save(employe);
            L.debug("Employee object saved.");
            L.info("End mettreAjourEmailByEmployeId().");
        }
    }

    @Transactional
    public void affecterEmployeADepartement(int employeId, int depId) {

        Optional<Departement> depManagedEntityOPT = deptRepoistory.findById(depId);
        if (depManagedEntityOPT.isPresent())
{
            Departement depManagedEntity = depManagedEntityOPT.get();

            Optional<Employe> employeManagedEntityOPT = employeRepository.findById(employeId);
            if (employeManagedEntityOPT.isPresent())
     {
                Employe employeManagedEntity = employeManagedEntityOPT.get();
                if (depManagedEntity.getEmployes() == null) {

                    List<Employe> employes = new ArrayList<>();
                    employes.add(employeManagedEntity);
                    depManagedEntity.setEmployes(employes);
                } else {

                    depManagedEntity.getEmployes().add(employeManagedEntity);
                }

                // à ajouter?
                deptRepoistory.save(depManagedEntity);
            }


        }


    }

    @Transactional
    public void desaffecterEmployeDuDepartement(int employeId, int depId) {
        Optional<Departement> dep = deptRepoistory.findById(depId);
        if(dep.isPresent()) {

            int employeNb = dep.get().getEmployes().size();
            for (int index = 0; index < employeNb; index++) {
                if (dep.get().getEmployes().get(index).getId() == employeId) {
                    dep.get().getEmployes().remove(index);
                    break;//a revoir
                }
            }
        }
    }

    // Tablesapce (espace disque)



    public void affecterContratAEmploye(int contratId, int employeId) {

        Optional<Contrat> contratManagedEntityOPT = contratRepoistory.findById(contratId);
        if(contratManagedEntityOPT.isPresent())
        {
            Contrat contratManagedEntity = contratManagedEntityOPT.get();
            Optional<Employe> employeManagedEntityOPT = employeRepository.findById(employeId);

            if (employeManagedEntityOPT.isPresent())
{
                Employe employeManagedEntity = employeManagedEntityOPT.get();
                contratManagedEntity.setEmploye(employeManagedEntity);
                contratRepoistory.save(contratManagedEntity);
            }

}

    }

    public String getEmployePrenomById(int employeId) {
        Optional<Employe> employeManagedEntityOPT = employeRepository.findById(employeId);
        if(employeManagedEntityOPT.isPresent()) {
            return employeManagedEntityOPT.get().getPrenom();
        }
        else {
            return null;
        }
    }

    public void deleteEmployeById(int employeId) {

        Optional<Employe> employeOPT = employeRepository.findById(employeId);
if(employeOPT.isPresent()) {
    Employe employe = employeOPT.get();
    //Desaffecter l'employe de tous les departements
    //c'est le bout master qui permet de mettre a jour
    //la table d'association
    for (Departement dep : employe.getDepartements()) {
        dep.getEmployes().remove(employe);
    }

    employeRepository.delete(employe);
}
    }

    public void deleteContratById(int contratId) {
       Optional<Contrat> contratManagedEntity = contratRepoistory.findById(contratId);
       if(contratManagedEntity.isPresent())
        contratRepoistory.delete(contratManagedEntity.get());

    }

    public int getNombreEmployeJPQL() {
        return employeRepository.countemp();
    }

    public List<String> getAllEmployeNamesJPQL() {
        return employeRepository.employeNames();

    }

    public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
        return employeRepository.getAllEmployeByEntreprisec(entreprise);
    }

    public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
        employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);

    }

    public void deleteAllContratJPQL() {
        employeRepository.deleteAllContratJPQL();
    }

    public float getSalaireByEmployeIdJPQL(int employeId) {
        return employeRepository.getSalaireByEmployeIdJPQL(employeId);
    }

    public Double getSalaireMoyenByDepartementId(int departementId) {
        return employeRepository.getSalaireMoyenByDepartementId(departementId);
    }

    public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
                                                         Date dateFin) {
        return timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
    }

    public List<Employe> getAllEmployes() {
        return (List<Employe>) employeRepository.findAll();
    }

}
