package tn.esprit.spring.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.repository.DepartementRepository;

@Service
public class DepartementServiceImpl implements IDepartementService {


	@Autowired
	DepartementRepository deptRepoistory;
	private static final Logger L = LoggerFactory.getLogger(DepartementServiceImpl.class);

	public List<Departement> getAllDepartements() {
		return (List<Departement>) deptRepoistory.findAll();
	}
	@Override
	public int ajouterDepartement(Departement dep) {
		L.info("Start addDepartement().");
		deptRepoistory.save(dep);
		L.info("End addDepartement().");
		return dep.getId();
		
	}
	
	@Override
	public void deleteDepartementById(int depId) {
		L.info("Start deleteDepartementById().");
		Optional<Departement> od = deptRepoistory.findById(depId);
		if (od.isPresent()) {
			deptRepoistory.delete(od.get());
			L.info("End deleteDepartementById().");
		}
	}

}
