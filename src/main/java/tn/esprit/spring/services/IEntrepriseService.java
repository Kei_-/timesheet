package tn.esprit.spring.services;

import java.util.List;

import tn.esprit.spring.entities.Entreprise;

public interface IEntrepriseService {

	int ajouterEntreprise(Entreprise entreprise);
	void affecterDepartementAEntreprise(int depId, int entrepriseId);
	List<String> getAllDepartementsNamesByEntreprise(int entrepriseId);
	void deleteEntrepriseById(int entrepriseId);
	Entreprise getEntrepriseById(int entrepriseId);
}
