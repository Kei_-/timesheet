package tn.esprit.spring.services;

import java.util.List;

import tn.esprit.spring.entities.Contrat;


public interface IContratService {
	
	
	List<Contrat> getAllContrats();

	public int ajouterContrat(Contrat contrat);
	public void deleteContratById(int contratId);






}
