package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.dto.DtoDepartement;
import tn.esprit.spring.services.IDepartementService;

public class RestControlDepartement {
	@Autowired
	IDepartementService idepartementservice;
	
	


	 	@PostMapping("/ajouterDepartement")
	 	@ResponseBody
		public int ajouterDepartement(@RequestBody DtoDepartement dtodep) {
	 		Departement dep = DtoDepartement.modelMap(dtodep);
		 idepartementservice.ajouterDepartement(dep);
		 return dep.getId();
		}
	 	
	 	  
	    @DeleteMapping("/deleteDepartementById/{iddept}") 
		@ResponseBody 
		public void deleteDepartementById(@PathVariable("iddept") int depId) {
	    	idepartementservice.deleteDepartementById(depId);

		}
}
