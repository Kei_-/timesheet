package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.dto.DtoContrat;
import tn.esprit.spring.services.IContratService;

@RestController
public class RestControllerContrat {



    @Autowired
    IContratService iContratService;


    @PostMapping("/ajouterContrat")
    @ResponseBody
    public int ajouterContrat(@RequestBody DtoContrat contrat) {
        Contrat c = DtoContrat.modelMap(contrat);
        iContratService.ajouterContrat(c);
        return c.getReference();
    }

    @DeleteMapping("/deleteContratById/{idcontrat}")
    @ResponseBody
    public void deleteContratById(@PathVariable("idcontrat")int contratId) {
        iContratService.deleteContratById(contratId);
    }
}
