package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.services.IDepartementService;

public class ControllerDepartement {
	@Autowired
	IDepartementService idepartementservice;
	public int ajouterDepartement(Departement dep) {
		return idepartementservice.ajouterDepartement(dep);
	}

	public void deleteDepartementById(int depId) {
		idepartementservice.deleteDepartementById(depId);

	}
}
