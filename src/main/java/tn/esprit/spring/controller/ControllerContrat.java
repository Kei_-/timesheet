package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.services.IContratService;


public class ControllerContrat {

    @Autowired
    IContratService iContrat;

    public IContratService getiContrat() {
        return iContrat;
    }

    public void setiContrat(IContratService iContrat) {
        this.iContrat = iContrat;
    }

    public int ajouterContrat(Contrat contrat) {
        iContrat.ajouterContrat(contrat);
        return contrat.getReference();
    }

    public void deleteContratById(int contratId) {
        iContrat.deleteContratById(contratId);
    }
}
